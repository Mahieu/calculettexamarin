﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculatrice
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public double currentValue = 0;
        public bool currentNumberIsEdited = false;
        public string lastOperator="";
        public double lastNumber = 0;
        public MainPage()
        {
            InitializeComponent();
        }
        async void OnNumberButtonClick(object sender, EventArgs args)
        {
            Button btn = (Button)sender;
            if (currentNumberLabel.Text != "0" && currentNumberLabel.Text != null && currentNumberIsEdited)
            {
                currentNumberLabel.Text += btn.Text;
            }
            else if (!currentNumberIsEdited)
            {
                currentNumberLabel.Text = btn.Text;
            }
            else
            {
                currentNumberLabel.Text += btn.Text;
            }
            currentNumberIsEdited = true;
        }
        async void OnOperationButtonClick(object sender, EventArgs args)
        {
            Button btn = (Button)sender;
            string operation = btn.Text;
            switch (operation) {
                case "=":
                    if(lastOperator != "")
                    {
                        if (lastNumber == 0)
                        {
                            lastNumber = double.Parse(currentNumberLabel.Text);
                            Regex rg = new Regex(@"[+-\/*]");
                            MatchCollection matchedOperators = rg.Matches(operationsLabel.Text);
                            lastOperator = matchedOperators[matchedOperators.Count - 1].Value;
                            operationsLabel.Text += currentNumberLabel.Text + " =";

                            calculate(lastNumber, lastOperator);
                            currentNumberLabel.Text = currentValue.ToString();

                        }
                        else
                        {
                            operationsLabel.Text = currentNumberLabel.Text + " " + lastOperator + " " + lastNumber + " =";
                            calculate(lastNumber, lastOperator);
                            currentNumberLabel.Text = currentValue.ToString();
                        }
                        currentNumberIsEdited = false;
                    }
                    break;
                case "+":
                    addOperation(operation);
                    break;
                case "-":
                    addOperation(operation);
                    break;
                case "/":
                    addOperation(operation);
                    break;
                case "*":
                    addOperation(operation);
                    break;
                case ",":
                    addComma();
                    break;
                case "CE":
                    currentNumberLabel.Text = "";
                    currentNumberIsEdited = true;
                    break;
                case "C":
                    operationsLabel.Text = "";
                    currentNumberLabel.Text = "";
                    currentValue = 0;
                    lastOperator = "";
                    lastNumber = 0;
                    currentNumberIsEdited = true;

                    break;
                case "D":
                    if (currentNumberLabel.Text.Length > 0)
                    {
                        currentNumberLabel.Text = currentNumberLabel.Text.Substring(0, currentNumberLabel.Text.Length - 1);
                    }
                    currentNumberIsEdited = true;
                    break;
                case "+/-":
                    operationsLabel.Text = "";
                    currentNumberLabel.Text = (0- double.Parse(currentNumberLabel.Text)).ToString();
                    break;

            }
        }
        private void addComma() {
            if (currentNumberLabel.Text != "" && !currentNumberLabel.Text.Contains(",")) {
                currentNumberLabel.Text += ",";
                currentNumberIsEdited = true;
            }
        }
        private void addOperation(string operation) {
            if (currentNumberLabel.Text != "") {
                if (operationsLabel.Text == "" || operationsLabel.Text == null)
                {
                    operationsLabel.Text = currentNumberLabel.Text + " " + operation;
                    currentValue = double.Parse(currentNumberLabel.Text);
                }
                else
                {
                    if (currentNumberIsEdited)
                    {
                        operationsLabel.Text += currentNumberLabel.Text;
                        calculate(double.Parse(currentNumberLabel.Text), lastOperator);
                        operationsLabel.Text += " " + operation;
                        currentNumberLabel.Text = currentValue.ToString();
                    }
                    else {
                        operationsLabel.Text= operationsLabel.Text.Substring(0,operationsLabel.Text.Length - 1) + operation;
                        
                    }
                }
                lastOperator = operation;
                lastNumber = 0;
            }
            currentNumberIsEdited = false;
        }
        private void calculate(double number, string operation) {
            switch (operation)
            {
                case "+":
                    currentValue += number;
                    break;
                case "-":
                    currentValue -= number;
                    break;
                case "/":
                    currentValue /= number;
                    break;
                case "*":
                    currentValue *= number;
                    break;
            }
        }
    }
}
